import React, { Component } from 'react';
import { Scene, Router, Drawer, Actions } from 'react-native-router-flux';
import { Dimensions, AsyncStorage } from 'react-native';
import { Root } from 'native-base'
import Login from './components/Login';
import Signup from './components/Signup';
import Department from './components/Department';
import ComplaintForm from './components/ComplaintForm';
import { TheDrawer } from './components/common'

export default class App extends Component {

  onBackPress() {
    if (Actions.state.index === 0 | Actions.currentScene == 'Check') {
      return false
    }
    Actions.pop();
    return true
  }

  render() {
    return (
      <Root>
        <Router
          backAndroidHandler={this.onBackPress}
        >
          <Scene key="root" hideNavbar>
            <Scene key="ComplaintForm" component={ComplaintForm} hideNavBar initial />

            {/* <Scene key="Check" component={Check} hideNavBar initial /> */}

            {/* <Scene key="main" type='reset' initial hideNavBar>
            <Scene key="Login" component={Login} hideNavBar />
            <Scene key="Signup" component={Signup} hideNavBar />
          </Scene> */}

            <Drawer
              hideNavBar
              key="drawer"
              contentComponent={TheDrawer}
              // drawerBackgroundColor={'rgba(255,255,255,.9)'}
              drawerWidth={Dimensions.get('window').width / 1.5}
              type="reset"
              drawerPosition={'right'}
            >
              <Scene key='user' hideNavBar >
                <Scene key="Department" component={Department} hideNavBar initial />
                <Scene key="ComplaintForm" component={ComplaintForm} hideNavBar />
              </Scene>
            </Drawer>
          </Scene>
        </Router>
      </Root>
    );
  }
}
