export const serverUrl = "http://venyoubook.com/api/";

export function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

export function getParsedDate(date) {
    date = date.split('-');
    var day = date[2].split(' ');

    var month = monthNumToName(date[1])

    return [(date[0]), month, (day[0]),date[1]];
}

function monthNumToName(monthnum) {
    var months = [
        'January', 'February', 'March', 'April', 'May',
        'June', 'July', 'August', 'September',
        'October', 'November', 'December'
        ];
    return months[monthnum - 1] || '';
}