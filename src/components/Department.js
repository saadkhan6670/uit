import React, { Component, Fragment } from 'react';
import { StyleSheet, Text, View, Image, Dimensions, ScrollView } from 'react-native';
import { Title, Button, Content, Container, Card, CardItem, Body, Fab } from "native-base";
import { Actions } from 'react-native-router-flux'
import { Structure } from './common'
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class Department extends Component {

    render() {
        return (

            <Structure headerText={'Screen Title'}  >
                <Content>
                    <Card>
                        <CardItem onPress={() => { Actions.ComplaintForm() }} style={{margin:3}}>
                            <Body>
                                <View >
                                    <View style={{ flexDirection: "row", alignItems: 'center', marginBottom: 10 }}>
                                        <Ionicons name="md-settings" color="#4E4E4E" size={40} />
                                        <View style={{ paddingLeft: 10 }}>
                                            <Text style={[styles.textColor, { fontSize: 20, fontWeight:'700' }]}>Department</Text>
                                            <Text style={{ color: '#4E4E4E', fontSize: 14 }}>Short Discription</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the first line</Text>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the Second line</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem onPress={() => { Actions.ComplaintForm() }} style={{margin:3}}>
                            <Body>
                                <View >
                                    <View style={{ flexDirection: "row", alignItems: 'center', marginBottom: 10 }}>
                                        <FontAwesome5 name="plug" color="#4E4E4E" size={40} />
                                        <View style={{ paddingLeft: 10 }}>
                                            <Text style={[styles.textColor, { fontSize: 20, fontWeight:'700' }]}>Department</Text>
                                            <Text style={{ color: '#4E4E4E', fontSize: 14 }}>Short Discription</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the first line</Text>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the Second line</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem onPress={() => { Actions.ComplaintForm() }} style={{margin:3}}>
                            <Body>
                                <View >
                                    <View style={{ flexDirection: "row", alignItems: 'center', marginBottom: 10 }}>
                                        <FontAwesome5 name="water" color="#4E4E4E" size={40} />
                                        <View style={{ paddingLeft: 10 }}>
                                            <Text style={[styles.textColor, { fontSize: 20, fontWeight:'700' }]}>Department</Text>
                                            <Text style={{ color: '#4E4E4E', fontSize: 14 }}>Short Discription</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the first line</Text>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the Second line</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem onPress={() => { Actions.ComplaintForm() }} style={{margin:3}}>
                            <Body>
                                <View >
                                    <View style={{ flexDirection: "row", alignItems: 'center', marginBottom: 10 }}>
                                        <FontAwesome5 name="key" color="#4E4E4E" size={40} />
                                        <View style={{ paddingLeft: 10 }}>
                                            <Text style={[styles.textColor, { fontSize: 20, fontWeight:'700' }]}>Department</Text>
                                            <Text style={{ color: '#4E4E4E', fontSize: 14 }}>Short Discription</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the first line</Text>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the Second line</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>

                    <Card>
                        <CardItem onPress={() => { Actions.ComplaintForm() }} style={{margin:3}}>
                            <Body>
                                <View >
                                    <View style={{ flexDirection: "row", alignItems: 'center', marginBottom: 10 }}>
                                        <Ionicons name="md-settings" color="#4E4E4E" size={40} />
                                        <View style={{ paddingLeft: 10 }}>
                                            <Text style={[styles.textColor, { fontSize: 20, fontWeight:'700' }]}>Department</Text>
                                            <Text style={{ color: '#4E4E4E', fontSize: 14 }}>Short Discription</Text>
                                        </View>
                                    </View>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the first line</Text>
                                    <Text style={[styles.DepartDisc, styles.textColor]}>This is some text on the Second line</Text>
                                </View>
                            </Body>
                        </CardItem>
                    </Card>
                </Content>
            </Structure>
        );
    }
}

const styles = StyleSheet.create({
    textColor: {
        color: '#4E4E4E'
    },
    DepartDisc: {
        fontSize: 15
    }

});
