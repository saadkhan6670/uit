import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, Platform } from 'react-native';
import {
    Header,
    Left,
    Body,
    Right,
    Title,
    Button,
    Icon,
} from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Feather from 'react-native-vector-icons/Feather';
import { Actions } from 'react-native-router-flux'

class HeaderComp extends Component {
    render() {

        return (
            <Header style={styles.headerStyle} androidStatusBarColor="#0750a4">

                <Left style={{ flex: 1}} >
                {this.props.headerText == 'My Profile' ? null : <Button onPress={() => Actions.pop()} transparent>
                        <Ionicons name='ios-arrow-back' size={20} color="#f7f7f7" />
                    </Button>}
                </Left>
                <Body style={styles.headerBody}>
                    <Title style={styles.headerTextStyle}>
                        {this.props.headerText}
                    </Title>
                </Body>
                <Right>
                    <Button transparent onPress={() => { Actions.drawerOpen() }}  >
                        <Feather name="menu" size={20} color="#f7f7f7" />
                    </Button>
                </Right>
            </Header>
        );
    }


};

const styles = StyleSheet.create({
    headerStyle: {
        backgroundColor: '#0750a4',
    },
    headerBody: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 3
    },
    headerTextStyle: {
        fontSize: 20,
        fontFamily: 'Montserrat-Bold'
    },
});

export { HeaderComp };
