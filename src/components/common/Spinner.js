import React from 'react';
import { Footer, FooterTab, Button, Text, Spinner } from 'native-base';
import { StyleSheet, View } from 'react-native';

class SpinnerComp extends React.Component {
    render() {
        return (
            <View style={styles.SpinnerView}>
                <Spinner color='#562F57' />
                {/* <Text style={styles.fontFaceMedium}>Getting your Data</Text> */}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    SpinnerView: { justifyContent: 'center', alignItems: 'center', height: '100%', width: '100%' },
    fontFaceMedium: { fontFamily: 'Montserrat-Medium', color: '#562F57', fontSize: 17 },
})

export { SpinnerComp }