import React from 'react';
import { Footer, FooterTab, Button, Text } from 'native-base';
import { StyleSheet, View } from 'react-native';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { Actions } from 'react-native-router-flux';
import Feather from 'react-native-vector-icons/Feather';

class FooterComp extends React.Component {

    render() {
        return (
            <Footer>
                <FooterTab style={styles.footerStyle}>
                    <Button
                        onPress={() => { Actions.MyAccount() }} vertical>
                        <MaterialIcons name="person-outline" size={22} color="#4C4C4C" />
                        <Text style={{ color: "#4C4C4C", paddingLeft: 1, paddingRight: 1 }}>Profile</Text>
                    </Button>
                    <Button onPress={() => { Actions.Vendors() }} vertical>
                        <AntDesign name="search1" size={22} color="#4C4C4C" />
                        <Text style={{ color: "#4C4C4C", paddingLeft: 1, paddingRight: 1 }}>Vendors</Text>
                    </Button>
                    <Button vertical>
                        <AntDesign name="shoppingcart" size={22} color="#4C4C4C" />
                        <Text style={{ color: "#4C4C4C", paddingLeft: 1, paddingRight: 1 }}>Store</Text>
                    </Button>
                    <Button
                        onPress={this.props.onPress}
                        vertical>
                        <Feather name="percent" size={22} color="#4C4C4C" />
                        <Text style={{ color: "#4C4C4C", paddingLeft: 1, paddingRight: 1 }} >Discount</Text>
                    </Button>
                    <Button onPress={() => { Actions.drawerOpen() }} vertical >
                        <MaterialIcons name="more-horiz" size={22} color="#4C4C4C" />
                        <Text style={{ color: "#4C4C4C", paddingLeft: 1, paddingRight: 1 }}>More</Text>
                    </Button>
                </FooterTab>
            </Footer>
        );
    }

}

const styles = StyleSheet.create({
    wrapper: {
        flex: 1
    },
    footerStyle: {
        backgroundColor: '#D9D9D9',
    },
    circleStyle: {

        width: 55,
        height: 55,
        borderRadius: 150 / 2,
        backgroundColor: '#713996',
        justifyContent: 'center',
        alignItems: 'center'
    },
    fontFace: { fontFamily: 'Montserrat' },
    fontFaceBold: { fontFamily: 'Montserrat-Bold' },
    fontFaceLight: { fontFamily: 'Montserrat-Light' },
    fontFaceMedium: { fontFamily: 'Montserrat-Medium' },
});

export { FooterComp }