import React, { Component } from 'react';
import { Container, } from "native-base";
import { HeaderComp, FooterComp } from './index'
import { Actions } from 'react-native-router-flux'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import AntDesign from 'react-native-vector-icons/AntDesign';

import {
    Text,
    StyleSheet,
    View,
    TouchableHighlight
} from 'react-native';


class Structure extends Component {


    constructor() {
        super();
        this.state = {
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3
        };
    }

    render() {

        return (
            <Container>
                <HeaderComp headerText={this.props.headerText} />
                <Container >
                    {this.props.children}
                </Container>
            
                {/* <FooterComp /> */}
            </Container>
        );
    }
}

export { Structure }


const styles = StyleSheet.create({
    fontFace: { fontFamily: 'Montserrat' },
    fontFaceBold: { fontFamily: 'Montserrat-Bold' },
    fontFaceLight: { fontFamily: 'Montserrat-Light' },
    fontFaceMedium: { fontFamily: 'Montserrat-Medium' },
});
