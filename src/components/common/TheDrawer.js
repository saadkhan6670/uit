import React from 'react';
import { StyleSheet, Text, View, AsyncStorage } from 'react-native';
import { Content, ListItem, Left, Body, Thumbnail, Card, CardItem } from "native-base";
import { Actions } from 'react-native-router-flux';
import AntDesign from 'react-native-vector-icons/AntDesign';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Feather from 'react-native-vector-icons/Feather'

class TheDrawer extends React.Component {

  handleScene() {
    AsyncStorage.removeItem('access_token')
    Actions.main();
  }


  render() {
    return (
      <Content>

        <View style={{ padding: 30, alignItems: 'center', justifyContent: 'center' }}>
          <View style={{top:9}}>
            <Thumbnail large source={require('../../images/sidenavAvatar.png')} style={{ height: 65, width: 65 }} />
          </View>
          <View style={{ borderColor: "#F0F0F0", backgroundColor: '#fff', borderWidth: 1, 
          padding: 13, alignItems: 'center', }}>

            <Text style={[styles.fontFaceBold, { color: "#484848", fontSize: 16, fontWeight: '700' }]}>Mr Random</Text>
            <Text style={[styles.fontFace, { color: "#484848", fontSize: 13 }]}>Director Operations</Text>
          </View>
        </View>
        <ListItem last icon>
          <Left >
            <FontAwesome5 name="tasks" color={'#484848'} size={19} />
          </Left>
          <Body>
            <Text style={[styles.fontFaceMedium, { color: "#484848", fontSize: 16 }]}>Task Log</Text>
          </Body >
        </ListItem>
        <ListItem last icon>
          <Left >
            <MaterialCommunityIcons name="account-key" color={'#484848'} size={19} />
          </Left>
          <Body>
            <Text style={[styles.fontFaceMedium, { color: "#484848", fontSize: 16 }]}>Account</Text>
          </Body >
        </ListItem>
        <ListItem last icon>
          <Left >
            <Feather name="map-pin" color={'#484848'} size={23} />
          </Left>
          <Body>
            <Text style={[styles.fontFaceMedium, { color: "#484848", fontSize: 16 }]}>Site Map</Text>
          </Body >
        </ListItem>
        <ListItem last icon>
          <Left >
            <Feather name="help-circle" color={'#484848'} size={19} />
          </Left>
          <Body>
            <Text style={[styles.fontFaceMedium, { color: "#484848", fontSize: 16 }]}>Help & Support</Text>
          </Body >
        </ListItem>
        <ListItem last icon>
          <Left >
            <SimpleLineIcons name="logout" color={'#484848'} size={19} />
          </Left>
          <Body>
            <Text style={[styles.fontFaceMedium, { color: "#484848", fontSize: 16 }]}>Logout</Text>
          </Body >
        </ListItem>

      </Content>
    );
  }
}

const styles = StyleSheet.create({
  scene: {
    flex: 1,
  },
  fontFace: { fontFamily: 'Montserrat' },
  fontFaceBold: { fontFamily: 'Montserrat-Bold' },
  fontFaceLight: { fontFamily: 'Montserrat-Light' },
  fontFaceMedium: { fontFamily: 'Montserrat-Medium' },
  sideHeading: { color: '#181818', fontSize: 18 },
  GuestView: { flexDirection: 'row', flex: 1, paddingLeft: 25, paddingRight: 25, paddingTop: 10 },
  GuestHeading: { flex: 0.7, justifyContent: 'center' },
});

export { TheDrawer };