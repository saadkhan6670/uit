import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, Linking } from 'react-native';
import { Container, Header, Content, Item, Input, Button, Icon, Body, Title, Left, Right } from "native-base";
import { Actions } from 'react-native-router-flux';

export default class Login extends Component {

    render() {
        return (
            <Content style={styles.container}>

                <Header androidStatusBarColor="#0750a4" style={{ backgroundColor: "#0750a4" }} >
                    <Left style={{ flex: 1 }} >
                        <Button onPress={() => { Actions[this.props.value]() }} transparent>
                            <Icon name='ios-arrow-round-back' />
                        </Button>
                    </Left>
                    <Body style={{ alignItems: 'center', justifyContent: 'center', flex: 3 }}>
                        <Title style={{
                            fontSize: 22,
                            fontFamily: 'Montserrat-Bold'
                        }}>Sign Up</Title>
                    </Body>
                    <Right style={{ opacity: 0 }}>
                        <Button transparent >

                        </Button>
                    </Right>
                </Header>

                <View style={[styles.InputView, { paddingTop: 70 }]}>
                    <Title style={styles.titleStyle}>Username</Title>
                    <Item >
                        <Input style={styles.Input} placeholder="Type your username here" placeholderTextColor="#9e9e9e" />
                    </Item>
                </View>

                <View style={styles.InputView}>
                    <Title style={styles.titleStyle}>Email address</Title>
                    <Item >
                        <Input style={styles.Input} placeholder="faraz@xyz.com" placeholderTextColor="#9e9e9e" />
                    </Item>
                </View>

                <View style={styles.InputView}>
                    <Title style={styles.titleStyle}>Password</Title>
                    <Item >
                        <Input secureTextEntry style={styles.Input} placeholder="**********" placeholderTextColor="#9e9e9e" />
                    </Item>
                </View>

                <View style={styles.InputView}>
                    <Title style={styles.titleStyle}>Confirm Password</Title>
                    <Item >
                        <Input secureTextEntry style={styles.Input} placeholder="**********" placeholderTextColor="#9e9e9e" />
                    </Item>
                </View>

                <View style={styles.buttonView}>
                    <Button style={styles.button} rounded>
                        <Text style={styles.buttonText}>Next</Text>
                    </Button>
                </View>
                <Text style={[styles.Input, { color: '#0750a4', paddingBottom: 25 }]}> Already have an account? <Text style={{ color: '#231f20' }} onPress={() => { Actions.Login() }}>Sign In</Text></Text>
            </Content>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f7f7f7',
    },
    InputView: {
        paddingTop: 25,
    },
    Input: {
        textAlign: 'center',
        color: "#FFFEFF"
    },
    titleStyle: {
        color: '#231f20'
    },
    buttonView: {
        paddingTop: 20,
        paddingBottom: 10,
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        color:'#0750a4',
        height: 50,
        width: 80 + "%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 18,
        color: "#f7f7f7"
    }
});