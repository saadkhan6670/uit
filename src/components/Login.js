import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableHighlight, AsyncStorage } from 'react-native';
import { Content, Item, Input, Button, Icon, Spinner } from "native-base";
import { Actions } from 'react-native-router-flux';
import axios from 'axios';
import { serverUrl } from '../utils/Constants'
export default class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: '',
            password: '',
            emptyHandle: '',
            disabled: false,
            secureTextEntry:true
        };
    }

    handleScene = () => {
        Actions.drawer();
        // if (this.state.username !== '' && this.state.password !== '') {

        //     this.setState({
        //         disabled: true
        //     })

        //     axios.post(`${serverUrl}login`, {
        //         email: this.state.username,
        //         password: this.state.password
        //     })
        //         .then((res) => {
        //             AsyncStorage.multiSet([['access_token', res.data.data.access_token]
        //                 , ['username', this.state.username], ['password', this.state.password]])
        //             Actions.drawer();
        //         })
        //         .catch((error) => {
        //             this.setState({
        //                 emptyHandle: error.response.data.errors.login,
        //                 disabled: false
        //             })
        //         })
        // }
        // else {

        //     this.setState({
        //         emptyHandle: 'username or password cannot be empty'
        //     })
        // }
    }

    handlesecureTextEntry = () =>{

        return this.setState({
            secureTextEntry : !this.state.secureTextEntry
        })

    }

    render() {
        return (
            <Content style={styles.container}>
                <StatusBar
                    backgroundColor="#0750a4"
                    barStyle="light-content"
                />
                <View style={styles.brandLogoContainer}>
                    <Image style={styles.brandLogo} 
                    source={require('../images/logo.png')}
                    resizeMode={'center'}
                     />
                </View>
                <Content>
                    <View style={styles.InputView}>
                        <Item >
                            <Input style={styles.Input} 
                            onChangeText={(text) => this.setState({ username: text, emptyHandle: '' })} 
                            placeholder="Email/Username" placeholderTextColor="#9e9e9e" />
                        </Item>
                    </View>
                    <View style={[styles.InputView, { paddingBottom: 50 }]}>
                        <Item>
                            <Input secureTextEntry={this.state.secureTextEntry} style={styles.Input} 
                            onChangeText={(text) => this.setState({ password: text, emptyHandle: '' })} 
                            placeholder="Password" placeholderTextColor="#9e9e9e" />
                            {/* <Ionicons name={this.state.secureTextEntry ?'ios-eye-off' : 'ios-eye'} size={25} color={this.state.secureTextEntry ? "#958595" : '#fff'} onPress={this.handlesecureTextEntry} /> */}
                        </Item>
                        <Text style={{ color: '#cb3837', textAlign: "center", padding: 15 }}>{this.state.emptyHandle}</Text>

                    </View>

                    <View style={{ bottom: 10 }} >
                        <TouchableHighlight disabled={this.state.disabled} 
                        underlayColor='transparent' onPress={this.handleScene} style={styles.buttonView}>
                            <Button disabled={this.state.disabled} style={styles.button} 
                            onPress={this.handleScene} rounded >
                                {this.state.disabled ? <Spinner color='#562F57' /> : <Text style={styles.buttonText}>Sign In</Text>}
                            </Button>
                        </TouchableHighlight>

                        <Text style={[styles.Input, { color: '#0750a4' }]}> Don't have an account? <Text style={{ color: '#231f20' }} onPress={() => { Actions.Signup({ value: 'Login' }) }}>Sign Up</Text></Text>
                    </View>
                </Content>
            </Content>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#f7f7f7',
    },
    brandLogoContainer: {
        paddingTop: 70,
        paddingBottom: 40,
        alignItems: 'center',
    },
    brandLogo: {
        height: 180,
        width: 180,
    },
    InputView: {
        paddingLeft: 50,
        paddingRight: 50,
        paddingTop: 5,
        paddingBottom: 8,
    },
    Input: {
        textAlign: 'center',
        color: '#fff'
    },
    buttonView: {
        paddingBottom: 20,
        flexDirection: "row",
        justifyContent: "center",
    },
    button: {
        color:'#0750a4',
        height: 50,
        width: 80 + "%",
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonText: {
        fontSize: 16,
        color: "#fffafa"
    }
});
