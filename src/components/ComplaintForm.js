import React, { Component, Fragment } from 'react';
import { StyleSheet } from 'react-native';
import { Content, List, ListItem, Text, Left, Right, Button, Picker, Item, Toast, Textarea, View } from "native-base";
import { Actions } from 'react-native-router-flux'
import { Structure } from './common'
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export default class ComplaintForm extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selected: "key1",
        };
    }

    onValueChange(value) {
        this.setState({
            selected: value
        });
    }

    render() {
        return (
            <Structure headerText={'Screen Title'}  >
                <Content>

                    <List>
                        <ListItem style={{ flex: 1 }} last>
                            <Left style={{ flex: 0.5 }}>
                                <Text style={{ fontSize: 18 }}>Building</Text>
                            </Left>
                            <Right style={{ flex: 0.5 }}>
                                <Item>
                                    <Picker
                                        note
                                        mode="dropdown"
                                        style={{ width: 120 }}
                                        selectedValue={this.state.selected}
                                        onValueChange={this.onValueChange.bind(this)}
                                    >
                                        <Picker.Item label="Wallet" value="key0" />
                                        <Picker.Item label="ATM Card" value="key1" />
                                        <Picker.Item label="Debit Card" value="key2" />
                                        <Picker.Item label="Credit Card" value="key3" />
                                        <Picker.Item label="Net Banking" value="key4" />
                                    </Picker>
                                </Item>
                            </Right>
                        </ListItem>

                        <ListItem style={{ flex: 1 }} last>
                            <Left style={{ flex: 0.5 }}>
                                <Text style={{ fontSize: 18 }}>Room</Text>
                            </Left>
                            <Right style={{ flex: 0.5 }}>
                                <Item>
                                    <Picker
                                        note
                                        mode="dropdown"
                                        style={{ width: 120 }}
                                        selectedValue={this.state.selected}
                                        onValueChange={this.onValueChange.bind(this)}
                                    >
                                        <Picker.Item label="Wallet" value="key0" />
                                        <Picker.Item label="ATM Card" value="key1" />
                                        <Picker.Item label="Debit Card" value="key2" />
                                        <Picker.Item label="Credit Card" value="key3" />
                                        <Picker.Item label="Net Banking" value="key4" />
                                    </Picker>
                                </Item>
                            </Right>
                        </ListItem>

                        <ListItem style={{ flex: 1 }} last>
                            <Left style={{ flex: 0.5 }}>
                                <Text style={{ fontSize: 18 }}>Floor</Text>
                            </Left>
                            <Right style={{ flex: 0.5 }}>
                                <Item>
                                    <Picker
                                        note
                                        mode="dropdown"
                                        style={{ width: 120 }}
                                        selectedValue={this.state.selected}
                                        onValueChange={this.onValueChange.bind(this)}
                                    >
                                        <Picker.Item label="Wallet" value="key0" />
                                        <Picker.Item label="ATM Card" value="key1" />
                                        <Picker.Item label="Debit Card" value="key2" />
                                        <Picker.Item label="Credit Card" value="key3" />
                                        <Picker.Item label="Net Banking" value="key4" />
                                    </Picker>
                                </Item>
                            </Right>
                        </ListItem>

                        <View style={{ margin: 20 }}>
                            <Text style={{ fontSize: 18 }}>Spcific Instruction</Text>
                            <Textarea  style={{borderRadius:4}} rowSpan={5} bordered placeholder="Textarea" />
                        </View>

                    </List>

                </Content>
            </Structure>
        );
    }
}

const styles = StyleSheet.create({

});
